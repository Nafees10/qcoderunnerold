unit QLib.QScript;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, QLib.Classes;

type
  //The actual class
  TQScript = class
  private
    Scr: TStringList;
    LineNo: Integer;
    function ExecFunc(Name: String; PosX, PosY: Integer): TQVar;
    function SolveArgs(const PosX,PosY: Integer): TQList;
  public
    Functions: TQScriptFunctions;
    constructor Create;
    destructor Destroy;

    procedure LoadFromFile(FName: String);
    procedure Execute;
  end;


implementation


{ETC functions}

//To get the index of the char where a bracket is ending
function GetDelimInd(S: String; const Start: Integer; EChar: Char): Integer;
var
  I, DCs: Integer;
  SChar: Char;
begin
   I := Start+1;
   SChar := S[Start];
   DCs := 1;
   while DCs>0 do
   begin
     if S[I]=SChar then DCs := DCs+1;
     if S[I]=EChar then DCs := DCs-1;
     I := I+1;
   end;
   Result := I-1;
end;

//I like to use Copy this way, instead of count, it has ToIndex
function CopyExt(S: String;Index, ToIndex: Integer): string;
begin
  Result := Copy(S,Index,(ToIndex-Index)+1);
end;

//Returns where the character occures next
function NextPos(S: string; Index: Integer): Integer;
var
  C: Char;
begin
  C := S[Index];
  Result := Index+1;
  while not(S[Result]=C) do
    Result:=Result+1;
end;

{/ETC functions}


constructor TQScript.Create;
begin
  Scr := TStringList.Create;
  LineNo := 0;
end;

destructor TQScript.Destroy;
begin
  Scr.Free;
end;

procedure TQScript.LoadFromFile(FName: String);
begin
  Scr.LoadFromFile(FName);
end;

//Execute the script
procedure TQScript.Execute;
var
  I,I2,Lnth: Integer;
  TmStr: String;
begin
  I := 0;
  while (I<Scr.Count) do
  begin
    I2 := 1;
    TmStr := '';
    Lnth := Length(Scr[I]);
    while (I2<=Lnth) do
    begin
      if (Scr[I][I2]='(') then
      begin
        ExecFunc(TmStr,I2,I);
        I2 := Lnth+1;
      end else
      begin
        TmStr := TmStr+Scr[I][I2];
        I2 := I2+1;
      end;
    end;
    I := I+1;
  end;
end;


//Call the script function with the arguments
function TQScript.ExecFunc(Name: String; PosX, PosY: Integer): TQVar;
var
  TmArg: TQList;
begin
  TmArg := TQList.Create;
  TmArg := Self.SolveArgs(PosX,PosY);
  Result := Functions.Call(Name,TmArg);
end;

//Seperate the arguments into a TQList
function TQScript.SolveArgs(const PosX,PosY: Integer): TQList;
var
  I, Lnth: Integer;
  Line, TmStr: String;
  Arg: TQVar;
  TmInt: Integer;
begin
  I := PosX+1;
  Line := Scr[PosY];
  Lnth := Length(Line);
  Result := TQList.Create;
  TmStr := '';
  while (I<=Lnth) do
  begin
    //if the argument is a string, don't look into it, just copy it
    if TmStr='''' then
    begin
      TmInt := NextPos(Line,I-1);
      TmStr := CopyExt(Line,I-1,TmInt);
      I := TmInt+1;
    end;

    //Check if the argument has been fully iterated through
    if not (TmStr='')
    and ((Line[I] = ',') or (Line[I] = ')')) then
    begin
      //if is a number, keep it a number
      if TmStr[1] in ['0'..'9']then
      begin
        Result.Add(StrToFloat(TmStr),'',True);
      end;
      //if is a string, keep it a string
      if TmStr[1]='''' then
      begin
        Result.Add(0,Copy(TmStr,2,Length(TmStr)-2),False);
      end;
      TmStr := '';
      I := I+1;
      Continue;
    end;

    //Check if argument is a function
    if (Line[I]='(')
    and not (TmStr='') then
    begin
      Arg := ExecFunc(TmStr,I,PosY);
      Result.Add(0,'',True);
      Result.List[Result.Taken] := Arg;
      I := GetDelimInd(Line,I,')')+2;
      TmStr := '';
      Continue;
    end;
    TmStr := TmStr+Line[I];
    I := I+1;
  end;
end;

end.

