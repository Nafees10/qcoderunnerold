unit QLib.Classes;

interface

uses SysUtils, Classes;

type
  //The data type for QSCript.
  TQVar = record
    R: Real;//To store a number
    S: String;//To store the string
    IsInt: Boolean;//To check which type it has
  end;

  //A list of TQVar
  TQList = class
  public
    Taken: Integer;//Number of Indexes of List it has taken
    Size: Integer;//Current number of indexes available, will allocate more if needed.
    List: array of TQVar;
    constructor Create;
    procedure Clear;
    function Add(R: Real; S: String; IsInt: Boolean): TQVar;
  end;

  TQScrFuncType = function(Args: TQList): TQVar of Object;//The QSCript function type

  //Class to add functions to the script engine( {$M+} is to add RTTI, which is must)
  {$M+}
  TQScriptFunctions = class
  private
    StackVal: TQList;//List containing values of variables
    StackVar: TStringList;//List containing names of variables
  published
    {basic functions}
    function DebugMessage(Args: TQList): TQVar;
    function SetVar(Args: TQList): TQVar;
    function GetVar(Args: TQList): TQVar;
    function Add(Args: TQList): TQVar;
    function Subtract(Args: TQList): TQVar;
    function Multiply(Args: TQList): TQVar;
    function Divide(Args: TQList): TQVar;
  public
    constructor Create;
    destructor Destroy;
    //This function must be present in the class, TQScript uses this function to
    //execute the script's functions
    function Call(MName: String; Args: TQList): TQVar; Virtual;
  end;

implementation

{TQList}

constructor TQList.Create;
begin
  SetLength(List,4);
  Size := 3;
  Taken := -1;
end;

procedure TQList.Clear;
begin
  SetLength(List,4);
  Size := 3;
  Taken := -1;
end;

function TQList.Add(R: Real; S: String; IsInt: Boolean): TQVar;
begin
  if Taken=Size then
    SetLength(List,Size+5);
  Taken := Taken+1;
  Result := List[Taken];

  if IsInt then
  begin
    List[Taken].R := R;
    List[Taken].IsInt := True;
  end else
  begin
    List[Taken].S := S;
    List[Taken].IsInt := False;
  end;
end;

{TQScriptFunctions}

constructor TQScriptFunctions.Create;
begin
  StackVal := TQList.Create;
  StackVar := TStringList.Create;
end;

destructor TQScriptFunctions.Destroy;
begin
  StackVal.Free;
  StackVar.Free;
end;

function TQScriptFunctions.Call(MName: String; Args: TQList): TQVar;
var
  Md: TMethod;
begin
  Md.Code := Self.MethodAddress(MName);
  Md.Data := Pointer(Self);
  Result := TQScrFuncType(Md)(Args);
  Args.Free;
end;

{basic script functions}
function TQScriptFunctions.DebugMessage(Args: TQList): TQVar;
begin
  if Args.List[0].IsInt then
  begin
    WriteLn(Args.List[0].R);
  end else
  begin
    WriteLn(Args.List[0].S);
  end;
end;

function TQScriptFunctions.SetVar(Args: TQList): TQVar;
var
  I: Integer;
begin
  I := StackVar.IndexOf(Args.List[0].S);
  if I>-1 then
  begin
    if Args.List[1].IsInt then
    begin
      StackVal.List[I].R := Args.List[1].R;
      StackVal.List[I].IsInt := True;
    end else
    begin
      StackVal.List[I].S := Args.List[1].S;
      StackVal.List[I].IsInt := False;
    end;
  end else
  begin
    StackVar.Add(Args.List[0].S);
    if Args.List[1].IsInt then
    begin
      StackVal.Add(Args.List[1].R,'',True);
    end else
    begin
      StackVal.Add(0,Args.List[1].S,False);
    end;
  end;
end;

function TQScriptFunctions.GetVar(Args: TQList): TQVar;
var
  I: Integer;
begin
  I := StackVar.IndexOf(Args.List[0].S);
  if I>-1 then
  begin
    if StackVal.List[I].IsInt then
    begin
      Result.R := StackVal.List[I].R;
      Result.IsInt := True;
    end else
    begin
      Result.S := StackVal.List[I].S;
      Result.IsInt := False;
    end;
  end;
end;

function TQScriptFunctions.Add(Args: TQList): TQVar;
var
  S: array [0..1] of String;
  R: array [0..1] of Real;
begin
  S[0] := Args.List[0].S;
  S[1] := Args.List[1].S;
  R[0] := Args.List[0].R;
  R[1] := Args.List[1].R;
  if Args.List[0].IsInt then
  begin
    Result.R := R[0]+R[1];
  end else
  begin
    Result.S := S[0]+S[1];
  end;
end;

function TQScriptFunctions.Subtract(Args: TQList): TQVar;
begin
  Result.R := Args.List[0].R-Args.List[1].R;
end;

function TQScriptFunctions.Multiply(Args: TQList): TQVar;
begin
  Result.R := Args.List[0].R*Args.List[1].R;
end;

function TQScriptFunctions.Divide(Args: TQList): TQVar;
begin
  Result.R := Args.List[0].R/Args.List[1].R;
end;

end.

