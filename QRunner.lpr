program QRunner;

uses
  QLib.Classes, QLib.QScript, SysUtils, Classes;

var
  Script: TQScript;//TQScript is the QScript engine.
  Funcs: TQScriptFunctions;//This is to add functions to your QSCript class

begin
  Script := TQScript.Create;
  Funcs := TQScriptFunctions.Create;
  //Remember that TQScript won't create/free the functions class itself!
  Script.Functions := Funcs;
  //Edit the following according to your needs
  Script.LoadFromFile(ExtractFileDir(ParamStr(0))+'/example.qod');
  //As it says, execute the script that was loaded.
  Script.Execute;
  Script.Free;
  //QSCript won't free the functions class itself!
  Funcs.Free;
  ReadLn;
end.

