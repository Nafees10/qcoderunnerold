Most of us just ignore this, that's why I've kept it short.

This code is "CC BY 4.0" licensed
For the complete license:
https://creativecommons.org/licenses/by/4.0/

In short:
You may copy, redistribute or change this code as long as you
provide appropriate credit and indicate if changes were made.

Thats it!
I recommend reading the complete license, it's not that long.
